#! /usr/bin/env python
##Python 2.x program for sentiment analysis-server
#With in the environment
#declare -x ROSLISP_PACKAGE_DIRECTORIES=""
#declare -x ROS_DISTRO="kinetic"
#declare -x ROS_ETC_DIR="/opt/ros/kinetic/etc/ros"
#declare -x ROS_MASTER_URI="http://localhost:11311"
#declare -x ROS_PACKAGE_PATH="/opt/ros/kinetic/share"
#declare -x ROS_ROOT="/opt/ros/kinetic/share/ros"
#declare -x ROS_VERSION="1"
# Software License Agreement (BSD License)
##Copyright (c) 2018, SANJO T VARGHESE, Licensed under the
#Educational Community License, Version 2.0 (the "License"); you may
#not use this file except in compliance with the License. You may
#obtain a copy of the License at

#http://www.osedu.org/licenses/ECL-2.0

#Unless required by applicable law or agreed to in writing,
#software distributed under the License is distributed on an "AS IS"
#BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
#or implied. See the License for the specific language governing
#permissions and limitations under the License

import rospy
import actionlib
import sentiment_analysis_2.msg

class SpeechAction(object):
	# create messages that are used to publish feedback/result
	_feedback = sentiment_analysis_2.msg.SpeechFeedback()
        _result = sentiment_analysis_2.msg.SpeechResult()
   
	def __init__(self, name):
        	self._action_name = name
        	self._as = actionlib.SimpleActionServer(self._action_name, sentiment_analysis_2.msg.SpeechAction, 			execute_cb=self.execute_cb,auto_start= False)
        	self._as.start()
      
	def execute_cb(self, goal):
		#helper variable
        	r = rospy.Rate(1)
        	success = True
		# publish info to the console for the user
        	rospy.loginfo('performing sentiment analysis of %s'%goal.text)
        	l=goal.text
        	l=l.split()

   		pcount=0
    		ncount=0
    		nucount=0
        	#positive words list
    		p= [
			'good','nice','beautiful','great','love','care','trust',
			'cared','help','helpful','helped','clean','excited','enjoy',
			'cure','donate','joy','knowle'
	   	   ]
		#negative words list
    		n= [
			"not","no","donot",
			"cannot","can't","never",
			"dark"	
                   ]
		#nutral words list
    		nu=[
			'hello','any','anyone','also','adequate','above',
			'absence','accept','accompany','actual','actually','advice',
			'anybody','anymore','anything','anywhere','how','are'
			'you','i','am'
	   	   ]
               
    		for i in range (0,len(l)):
			 # check that preempt has not been requested by the client
			if self._as.is_preempt_requested():
                		rospy.loginfo('%s: Preempted' % self._action_name)
                		self._as.set_preempted()
                		success = False
				
			if l[i] in p:
       				print ("item found in p")
       				pcount=pcount+1
      			if l[i] in n:
       				print ("found in n")
       				ncount=ncount+1
      			if l[i] in nu:
       				print ("found in nu")
       				nucount=nucount+1
      			#else:
       				#print("item not found in list")
		print pcount
 		print ncount
 		print nucount

    		if (pcount>=ncount) and (pcount>=nucount):
     			large=pcount
     			self._feedback.sequence=('It is a positive sentiment' )
           		self._as.publish_feedback(self._feedback)
                	r.sleep()

    		elif (ncount>=pcount) and (ncount>=nucount):
     			large=ncount
     			self._feedback.sequence=('It is a negative sentiment')
           		self._as.publish_feedback(self._feedback)
     			r.sleep()
     
    		else:
     			large=nucount
     			self._feedback.sequence=('It is a neutral sentiment')
           		self._as.publish_feedback(self._feedback)
     			r.sleep()
          
        	if success:
            		self._result.sequence = self._feedback.sequence
			rospy.loginfo(self._result.sequence)
            		rospy.loginfo('%s: Succeeded' % self._action_name)
            		self._as.set_succeeded(self._result)
        
if __name__ == '__main__':
	rospy.init_node('speech')
        server = SpeechAction(rospy.get_name())
        rospy.spin()
