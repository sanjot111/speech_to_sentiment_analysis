#! /usr/bin/env python
##Python 2.x program for sentiment analysis-client
#With in the environment
#declare -x ROSLISP_PACKAGE_DIRECTORIES=""
#declare -x ROS_DISTRO="kinetic"
#declare -x ROS_ETC_DIR="/opt/ros/kinetic/etc/ros"
#declare -x ROS_MASTER_URI="http://localhost:11311"
#declare -x ROS_PACKAGE_PATH="/opt/ros/kinetic/share"
#declare -x ROS_ROOT="/opt/ros/kinetic/share/ros"
#declare -x ROS_VERSION="1"
# Software License Agreement (BSD License)
##Copyright (c) 2018, SANJO T VARGHESE, Licensed under the
#Educational Community License, Version 2.0 (the "License"); you may
#not use this file except in compliance with the License. You may
#obtain a copy of the License at

#http://www.osedu.org/licenses/ECL-2.0

#Unless required by applicable law or agreed to in writing,
#software distributed under the License is distributed on an "AS IS"
#BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
#or implied. See the License for the specific language governing
#permissions and limitations under the License

import sys
import rospy
import speech_recognition as sr
from sentiment_analysis.srv import *
from gtts import gTTS
import os
from std_msgs.msg import String
def sentiment_analysis_client(x):
    	rospy.wait_for_service('sentiment_analysis')
    	try:
        	sentiment_analysis = rospy.ServiceProxy('sentiment_analysis', SentimentAnalysis)
        	resp1 = sentiment_analysis(x)
        	return resp1.sentiment
    	except rospy.ServiceException, e:
        	print "Service call failed: %s"%e

def usage():
    	return "%s [x]"%sys.argv[0]

if __name__ == "__main__":
	rospy.init_node('sentiment_analysis_client')
    	r = sr.Recognizer()
    	#r.energy_threshold =48000
    	with sr.Microphone() as source:
    		#r.adjust_for_ambient_noise(source)
    		print("Say something!")
    		audio = r.listen(source)
    		print "Recognising........."
    	try:
    		print("Google Speech Recognition thinks you said " + r.recognize_google(audio))
        	text=r.recognize_google(audio)
    	except sr.UnknownValueError:
    	 	print("Google Speech Recognition could not understand audio")
    	except sr.RequestError as e:
    	 	print("Could not request results from Google Speech Recognition service; {0}".format(e))
    	print "Requesting Server........"
    	textt=sentiment_analysis_client(text)
    	print textt
	#google api for audio output function
    	tts = gTTS(text=textt, lang='en')
    	tts.save("good.mp3")
    	os.system("mpg321 good.mp3")

        
    
