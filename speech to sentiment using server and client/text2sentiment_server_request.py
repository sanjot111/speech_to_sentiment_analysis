#! /usr/bin/env python
##Python 2.x program for sentiment analysis-server
#With in the environment
#declare -x ROSLISP_PACKAGE_DIRECTORIES=""
#declare -x ROS_DISTRO="kinetic"
#declare -x ROS_ETC_DIR="/opt/ros/kinetic/etc/ros"
#declare -x ROS_MASTER_URI="http://localhost:11311"
#declare -x ROS_PACKAGE_PATH="/opt/ros/kinetic/share"
#declare -x ROS_ROOT="/opt/ros/kinetic/share/ros"
#declare -x ROS_VERSION="1"
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
##Copyright (c) 2018, SANJO T VARGHESE, Licensed under the
#Educational Community License, Version 2.0 (the "License"); you may
#not use this file except in compliance with the License. You may
#obtain a copy of the License at

#http://www.osedu.org/licenses/ECL-2.0

#Unless required by applicable law or agreed to in writing,
#software distributed under the License is distributed on an "AS IS"
#BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
#or implied. See the License for the specific language governing
#permissions and limitations under the License


from sentiment_analysis.srv import *
import rospy
from gtts import gTTS
import os

def handle_sentiment_analysis(req):
   	text=req.a
	a=text.split()
    	pcount=0
    	ncount=0
	nucount=0
    	#positive words list
    	p=[
		'good','nice','beautiful','great','love','care','trust',
		'cared','help','helpful','helped','clean','excited','enjoy',
		'cure','donate','joy','knowle'
	  ]	   
	#negative words list
    	n=[
		"not","no","donot",
		"cannot","can't","never",
	  	"dark"
	  ]
	nu=[
		'hello','any','anyone','also','adequate','above',
		'absence','accept','accompany','actual','actually','advice',
		'anybody','anymore','anything','anywhere'
	   ]	

    	for i in range (0,len(a)):
		if a[i] in p:
       			print ("item found in positive")
       			pcount=pcount+1
      		if a[i] in n:
       			print ("found in negative")
       			ncount=ncount+1
      		if a[i] in nu:
       			print ("found in neutral")
 			nucount=nucount+1
	print pcount
	print ncount
	print nucount
	
	if (pcount>=ncount) and (pcount>=nucount):
			result="you sounded positive"
			return SentimentAnalysisResponse(result)
	elif (ncount>=pcount) and (ncount>=nucount):
			result="you sounded negative"
			return SentimentAnalysisResponse(result)
			print result
	else:
			result="you sounded neutral"
			return SentimentAnalysisResponse(result)
			print result
		
def sentiment_analysis_server():
    	rospy.init_node('sentiment_analysis_server')
    	result = rospy.Service('sentiment_analysis', SentimentAnalysis, handle_sentiment_analysis)
    	rospy.spin()

if __name__ == "__main__":
    	sentiment_analysis_server()
