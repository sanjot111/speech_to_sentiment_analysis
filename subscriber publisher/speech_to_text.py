#!/usr/bin/env python
##Python 2.x program to convert speech to sentiment
#With in the environment
#declare -x ROSLISP_PACKAGE_DIRECTORIES=""
#declare -x ROS_DISTRO="kinetic"
#declare -x ROS_ETC_DIR="/opt/ros/kinetic/etc/ros"
#declare -x ROS_MASTER_URI="http://localhost:11311"
#declare -x ROS_PACKAGE_PATH="/opt/ros/kinetic/share"
#declare -x ROS_ROOT="/opt/ros/kinetic/share/ros"
#declare -x ROS_VERSION="1"
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
##Copyright (c) 2018, sanjo t varghese, Licensed under the
#Educational Community License, Version 2.0 (the "License"); you may
#not use this file except in compliance with the License. You may
#obtain a copy of the License at

#http://www.osedu.org/licenses/ECL-2.0

#Unless required by applicable law or agreed to in writing,
#software distributed under the License is distributed on an "AS IS"
#BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
#or implied. See the License for the specific language governing
#permissions and limitations under the License.

import rospy
from std_msgs.msg import String
import speech_recognition as sr
# obtain audio from the microphone

def speech2text():
	##Speech recognition function
	r = sr.Recognizer()
	#Using microphone as the source of voice

    	with sr.Microphone() as source:
	    	print("Say something!")
		#Reads the audio file
	    	audio = r.listen(source)
		# for testing purposes, we're just using the default API key
	    	# to use another API key, use `r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")`
	    	# instead of `r.recognize_google(audio)`
	    	print("Google Speech Recognition thinks you said " + r.recognize_google(audio))
	    	text=r.recognize_google(audio)
	    	print text
	#Creating publisher node with topic name 'text_path'
    	pub = rospy.Publisher('text_path', String, queue_size=10)
    	rospy.init_node('speech_to_text', anonymous=True)
    	rate = rospy.Rate(10) # 10hz

	#Execute program until there is an interrupt ctrl-c  
    	while not rospy.is_shutdown():
        	rospy.loginfo(text)
        	pub.publish(text)
        	rate.sleep()
# Start main function
if __name__ == '__main__':
    	try:
        	speech2text()
    	except rospy.ROSInterruptException:
        	pass
