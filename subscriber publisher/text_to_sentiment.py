#1/usr/bin/env python
##Python 2.x program to convert text to sentiment
#With in the environment
#declare -x ROSLISP_PACKAGE_DIRECTORIES=""
#declare -x ROS_DISTRO="kinetic"
#declare -x ROS_ETC_DIR="/opt/ros/kinetic/etc/ros"
#declare -x ROS_MASTER_URI="http://localhost:11311"
#declare -x ROS_PACKAGE_PATH="/opt/ros/kinetic/share"
#declare -x ROS_ROOT="/opt/ros/kinetic/share/ros"
#declare -x ROS_VERSION="1"
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
##Copyright (c) 2018, sanjo t varghese, Licensed under the
#Educational Community License, Version 2.0 (the "License"); you may
#not use this file except in compliance with the License. You may
#obtain a copy of the License at

#http://www.osedu.org/licenses/ECL-2.0

#Unless required by applicable law or agreed to in writing,
#software distributed under the License is distributed on an "AS IS"
#BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
#or implied. See the License for the specific language governing
#permissions and limitations under the License.

import rospy
from std_msgs.msg import String
from gtts import gTTS
import os
#function which gives the recognised sentiment as audio output
def text(sentiment):
    	tts = gTTS(sentiment, lang='en')
    	tts.save("good.mp3")
    	os.system("mpg321 good.mp3")
# function for displaying the subscribed text from the sentiment recognition package
def callback(data):
    	rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data.data)
    	string=data.data
    	list_string=string.split()
	#positive words list
    	p= [
		"good","fine","nice",
		"do","fun","hai",
		"how","hi","morning",
		"hello"
	   ]
	#negative words list
    	n= [
		"bad","not","dirty",
		"devil","evil"
				
           ]
	#nutral words list
    	nu=[
		"he","she","is",
		"name","i","am",
		"a","boy","girl",
		"are","you"
	   ]
	#positive,negative and nutral words count
    	pcount=0
    	ncount=0
    	nucount=0
    	for temp in list_string:
      		if temp in p:
       			print ("item found in p")
       			pcount=pcount+1
      		elif temp in n:
       			print ("found in n")
       			ncount=ncount+1
      		elif temp in nu:
       			print ("found in nu")
       			nucount=nucount+1
      		else:
       			print("item not found in list")
    	if (pcount>=ncount) and (pcount>=nucount):
     		large=pcount
     		print ("your speech  is positive contains %d positive words ",large)
     
     		sentiment="your speech is positive"
     		text(sentiment)
    	elif (ncount>=pcount) and (ncount>=nucount):
     		large=ncount
     		print ("your speech  is negative contains %d negative words ",large)
     		sentiment="your speech is negative"
     		text(sentiment)
     
    	else:
     		large=nucount
     		print ("your speech  is neutral with %d  neutral words",large) 
     		sentiment="your speech is neutral"
     		text(sentiment)
def text2sentiment():
	# In ROS, nodes are uniquely named. If two nodes with the same
    	# name are launched, the previous one is kicked off. The
    	# anonymous=True flag means that rospy will choose a unique
    	# name for our 'listener' node so that multiple listeners can
    	# run simultaneously.
   	rospy.init_node('text_to_sentiment', anonymous=True)
	#Creating Subscriber node with topic name'text_path'
    	rospy.Subscriber('text_path', String, callback)
	# spin() simply keeps python from exiting until this node is stopped
    	rospy.spin()

if __name__ == '__main__':
    	text2sentiment()
